package scala
package scalanative
package nscxxplugin

import dotty.tools.dotc.ast.tpd
import dotty.tools.dotc.core.Contexts.Context
import dotty.tools.dotc.core.Names.TermName
import dotty.tools.dotc.core.Symbols.{MutableSymbolMap, Symbol}
import dotty.tools.dotc.plugins.PluginPhase
import dotty.tools.dotc.transform.{Constructors, Pickler}

object GatherCxxLayout:
    val name = "scalanative-cxx-gather-layout"

class GatherCxxLayout(val layout: MutableSymbolMap[List[(TermName, Symbol)]]) extends PluginPhase:
    import tpd.*

    val phaseName = GatherCxxLayout.name

    override val runsAfter = Set(Pickler.name)
    override val runsBefore = Set(Constructors.name)

    private val defs = Definitions()

    override def prepareForTemplate(tree: Template)(using ctx: Context): Context =
        if !(tree.symbol.owner.hasAnnotation(defs.externCxxAnnotationClass)) then return ctx

        val elements = tree.body
            .collect({ case valDef: ValDef => valDef })
            .filter({ _.rhs.symbol.showFullName == defs.externAnnotationClass.showFullName })
            .map({ valDef => (valDef.name, valDef.tpt.typeOpt.classSymbol) })

        layout(tree.symbol.owner) = elements

        ctx
