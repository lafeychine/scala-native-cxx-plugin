package scala
package scalanative
package nscxxplugin

import dotty.tools.dotc.core.Names.TermName
import dotty.tools.dotc.core.Symbols.{MutableSymbolMap, Symbol}
import dotty.tools.dotc.plugins.{PluginPhase, StandardPlugin}

class ScalaNativeCxxPlugin extends StandardPlugin:
    val name = "scalanative-cxx"
    val description = "Scala Native: C++ support"

    def init(options: List[String]): List[PluginPhase] =
        val layout = MutableSymbolMap[List[(TermName, Symbol)]]()

        List(GatherCxxLayout(layout), UnfoldCxx(layout))
