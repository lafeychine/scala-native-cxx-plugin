package scala
package scalanative
package nscxxplugin

import dotty.tools.dotc.core.Contexts.Context
import dotty.tools.dotc.core.Symbols.{requiredClassRef, ClassSymbol}
import dotty.tools.dotc.core.Types.TypeRef

class Definitions:

    def externCxxAnnotationType(using Context): TypeRef =
        requiredClassRef("scala.scalanative.unsafe.cxx.externCxx")

    def externCxxAnnotationClass(using Context): ClassSymbol =
        externCxxAnnotationType.symbol.asClass

    def externAnnotationType(using Context): TypeRef =
        requiredClassRef("scala.scalanative.unsafe.extern")

    def externAnnotationClass(using Context): ClassSymbol =
        externAnnotationType.symbol.asClass

    def nameAnnotationType(using Context): TypeRef =
        requiredClassRef("scala.scalanative.unsafe.name")

    def nameAnnotationClass(using Context): ClassSymbol =
        nameAnnotationType.symbol.asClass
