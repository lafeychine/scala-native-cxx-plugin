package scala
package scalanative
package nscxxplugin

import dotty.tools.dotc.ast.tpd
import dotty.tools.dotc.core.Annotations.Annotation
import dotty.tools.dotc.core.Constants.Constant
import dotty.tools.dotc.core.Contexts.Context
import dotty.tools.dotc.core.Names.TermName
import dotty.tools.dotc.core.Symbols.{MutableSymbolMap, Symbol}
import dotty.tools.dotc.core.Types.ConstantType
import dotty.tools.dotc.plugins.PluginPhase
import dotty.tools.dotc.util.Property

object UnfoldCxx:
    val name = "scalanative-cxx"

class UnfoldCxx(val layout: MutableSymbolMap[List[(TermName, Symbol)]]) extends PluginPhase:
    import tpd.*

    val phaseName = UnfoldCxx.name

    override val runsAfter = Set(GatherCxxLayout.name)
    override val runsBefore = Set("scalanative-genNIR")

    private val defs = Definitions()

    private val _key = Property.Key[Boolean]

    override def prepareForTypeDef(tree: TypeDef)(using ctx: Context): Context =
        if !(tree.symbol.hasAnnotation(defs.externCxxAnnotationClass)) then return ctx

        val template = tree.rhs match
            case template: Template => template
            case _                  => return ctx

        template.body
            .collect({ case defDef: DefDef => defDef })
            .filter({ _.rhs.symbol.showFullName == defs.externAnnotationClass.showFullName })
            .foreach({ _.putAttachment(_key, true) })

        tree.putAttachment(_key, true)

        ctx

    override def transformTypeDef(tree: tpd.TypeDef)(using Context): tpd.Tree =
        if !tree.hasAttachment(_key) then return tree

        tree.symbol.addAnnotation(Annotation(defs.externAnnotationClass, List(), tree.span))
        tree

    override def transformDefDef(tree: DefDef)(using Context): Tree =
        if !tree.hasAttachment(_key) then return tree

        val constant = Constant("_ZN3FooC2Ev") // Currently, this is just an example
        val mangledNameAnnotation =
            Annotation(defs.nameAnnotationType, Literal(constant).withType(ConstantType(constant)), tree.span)

        tree.symbol.addAnnotation(mangledNameAnnotation)
        tree
