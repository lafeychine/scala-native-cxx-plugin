package dotty.tools.dotc
package printer

import ast.untpd
import core.Contexts.Context
import core.Symbols.{defn, Symbol}
import printing.RefinedPrinter
import printing.Texts.Text

/* This file is a custom version from RefinedPrinter in dotty.
 *
 * The main objective is to get two equivalent inputs to produce the same output.
 *
 * @see: dotty/compiler/src/dotty/tools/dotc/printing/RefinedPrinter.scala */
class DeterministicRefinedPrinter(private val ctx: Context, private val removeAnnotations: Boolean) extends RefinedPrinter(ctx):

    private val annotationsToRemove = List("externCxx")

    override def coloredStr(text: String, color: String): String = text
    override def coloredText(text: Text, color: String): Text = text

    override def dropAnnotForModText(sym: Symbol): Boolean =
        sym == defn.BodyAnnot || sym == defn.SourceFileAnnot

    /* Annotations are not ordered. Therefore, annotations are sorted by name. */
    override def modText(mods: untpd.Modifiers, sym: Symbol, kw: String, isType: Boolean): Text =
        sym.annotations =
            sym.annotations.filter(annot => stripAnnotation(annot.symbol.name.toString)).sortBy(_.symbol.name.toString)

        super.modText(
            mods.copy(annotations =
                mods.annotations.filter(annot => stripAnnotation(annot.symbol.name.toString)).sortBy(_.symbol.name.toString)
            ),
            sym,
            kw,
            isType
        )

    private def stripAnnotation(annotation: String): Boolean =
        !removeAnnotations || !annotationsToRemove.contains(annotation)
