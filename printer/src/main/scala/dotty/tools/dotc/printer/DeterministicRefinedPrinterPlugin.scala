package dotty.tools.dotc
package printer

import core.Contexts.FreshContext
import plugins.{PluginPhase, StandardPlugin}

class DeterministicRefinedPrinterPlugin extends StandardPlugin:
    val name = "deterministic-printer"
    val description = "Deterministic refined printer, to compare TASTy output"

    override val optionsHelp: Option[String] = Some(s"""
        |  -P:$name:strip-cxx-annotations
        |     Remove C++ specific annotations from the output.
        """.stripMargin)

    def init(options: List[String]): List[PluginPhase] =
        List(DeterministicRefinedPrinterPhase(options.contains("strip-cxx-annotations")))
