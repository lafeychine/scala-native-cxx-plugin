package dotty.tools.dotc
package printer

import dotty.tools.dotc.core.Contexts.FreshContext
import dotty.tools.dotc.plugins.PluginPhase
import dotty.tools.dotc.parsing.Parser

class DeterministicRefinedPrinterPhase(private val removeAnnotations: Boolean) extends PluginPhase:
    override val runsBefore = Set(Parser.name)

    val phaseName = "set-deterministic-printer"

    override def initContext(ctx: FreshContext): Unit =
        ctx.setPrinterFn(ctx => DeterministicRefinedPrinter(ctx, removeAnnotations))
