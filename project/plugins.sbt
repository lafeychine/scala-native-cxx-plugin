/* Build */
addSbtPlugin("org.scala-native" % "sbt-scala-native" % "0.4.15")

/* Lint */
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.5.2")
